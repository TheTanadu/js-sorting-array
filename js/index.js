// Creating random array 14 items long with numbers from 1 to 40.
let randomArr = Array.from({length: 10}, () => Math.floor(Math.random() * 140+1));

if(!(randomArr.length == 0)) console.log("Our array: " + randomArr);

// Call function
sort(randomArr);

// Function to sort it.
function sort(taken){
  let secArr = [];
  do{
    let a = "I don't have nothing in my array.";
    
    if(taken.length > 0){
      let a = Math.min.apply(null, taken); // Take lowest number in array
      secArr.push(a); // Add this number to new empty array
      let index = taken.indexOf(a); //Find a in array
      if (index > -1) {
    taken.splice(index, 1); //Delete index in array
      }
    }else{console.log(a)} //If is nothing in array, tell it
    
  }while(taken.length > 0); //Do sort while is anything in original array
  
  if(secArr.length > 0) console.log("Our sorted array: " + secArr);
}